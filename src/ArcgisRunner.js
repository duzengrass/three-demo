import * as THREE from 'three'; 
import { loadModules,loadCss, setDefaultOptions } from 'esri-loader';

export default class ArcgisRunner {

  constructor(container,seedScene){
    this.container = container || document.getElementById('container-3d') || 'container-3d';
    this.renderer=null;   
    this.scene = new THREE.Scene(); 
    this.camera = new THREE.PerspectiveCamera(); 
    this.seedScene = seedScene;  
    this.axesHelper = new THREE.AxesHelper(500);  

    this.view=null;  
    this.modelPosition={x:112.585484, y:27.872235, z: 0};
    this.extent=null;
  }

  _init(){
    setDefaultOptions({ css: true });
    //loadCss('https://js.arcgis.com/4.14/esri/themes/dark/main.css')
    loadModules([
      "esri/Map",
      "esri/views/SceneView",
      "esri/views/3d/externalRenderers",
      "esri/geometry/SpatialReference",
      "esri/widgets/BasemapGallery",
      'esri/geometry/Extent',
      'esri/geometry/Point' 
    ])
    .then(([
      ArcGISMap,
      SceneView,
      externalRenderers,
      SpatialReference,
      BasemapGallery, 
      Extent,
      Point
    ]) => {  
        this.externalRenderers=externalRenderers;
        this.SpatialReference=SpatialReference;  

        // Create a map 
        var map = new ArcGISMap({
          basemap: 'satellite',//'satellite',//'osm',//
          //ground: "world-elevation" 
        });

        // Create a SceneView 
        this.view = new SceneView({
          container: this.container,
          map: map, 
        });  

        this.extent= new Extent({
          xmin:112.579,
          xmax:112.591,
          ymax:28.876,
          ymin: 27.869
        });
        this.view.on('click',(event)=>{
          var lat = Math.round(event.mapPoint.latitude * 1000) / 1000;
          var lon = Math.round(event.mapPoint.longitude * 1000) / 1000;
 
          if (!this.extent.contains(new Point({longitude:lon,latitude:lat}))){
            return;
          }

          this.view.hitTest(event).then((response)=>{     
            this.view.popup.open({
              // Set the popup's title to the coordinates of the location
              title: "余家露天矿: [" + lon + ", " + lat + "]",
              content:popupDiv,//'当期开采量: 169313.59立方米',
              location: event.mapPoint, // Set the location of the popup to the clicked location,
            });
          }); 
        });
        // register the external renderer
        this.externalRenderers.add(this.view, this);

        this.view.when(()=>{ 
          setTimeout(()=>{
            this.view.goTo({
              position: {
                ...this.modelPosition, 
                z: this.modelPosition.z+500*5 ,  
              },
              heading: 0,
              tilt: 0
            });
          },5000);
        });
 
    })
    .catch(err => {
      // handle any errors
      console.error(err);
    });   
  }

  start(){ 
    this._init();
  }

  /**
   * Setup function, called once by the ArcGIS JS API.
   */
  setup(context){
    // initialize the three.js renderer 
    this.renderer = new THREE.WebGLRenderer({
      context: context.gl,
      premultipliedAlpha: false
    });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setViewport(0, 0, this.view.width, this.view.height);

    // prevent three.js from clearing the buffers provided by the ArcGIS JS API.
    this.renderer.autoClearDepth = false;
    this.renderer.autoClearStencil = false;
    this.renderer.autoClearColor = false;

    // The ArcGIS JS API renders to custom offscreen buffers, and not to the default framebuffers.
    // We have to inject this bit of code into the three.js runtime in order for it to bind those
    // buffers instead of the default ones.
    var originalSetRenderTarget = this.renderer.setRenderTarget.bind(
      this.renderer
    );
    this.renderer.setRenderTarget = function(target) {
      originalSetRenderTarget(target);
      if (target == null) {
        context.bindRenderTarget();
      }
    };  

    this.scene.add(this.seedScene);
    this.scene.add(this.axesHelper); 

    // cleanup after ourselfs
    context.resetWebGLState(); 
  }

  /** 
   * Executed in every frame to draw geometry to the canvas. Tasks done in this function:
   * Update the state of dynamic resources
   * Set the WebGL state as needed for rendering
   * Issue WebGL draw calls
   * @param {*} context 
   */
  render(context){
    // update camera parameters 
    var cam = context.camera;

    this.camera.position.set(cam.eye[0], cam.eye[1], cam.eye[2]);
    this.camera.up.set(cam.up[0], cam.up[1], cam.up[2]);
    this.camera.lookAt(
      new THREE.Vector3(cam.center[0], cam.center[1], cam.center[2])
    );

    // Projection matrix can be copied directly
    this.camera.projectionMatrix.fromArray(cam.projectionMatrix);

    // update position 
    if (this.seedScene) { 
      let posEst = [this.modelPosition.x, this.modelPosition.y,0]; 

      // for the region, we position a torus slightly under ground
      // the torus also needs to be rotated to lie flat on the ground
      //posEst = [posEst[0], posEst[1], -450 * 1000];

      var transform = new THREE.Matrix4();  

      transform.fromArray(
        this.externalRenderers.renderCoordinateTransformAt(
          this.view,
          [posEst[0], posEst[1], 80],
          this.SpatialReference.WGS84,
          //new this.SpatialReference({wkid:4490}),
          new Array(16)
        )
      );

      transform.decompose(
        this.seedScene.position,
        this.seedScene.quaternion,
        this.seedScene.scale
      );

      transform.decompose(
        this.axesHelper.position,
        this.axesHelper.quaternion,
        this.axesHelper.scale
      );
    }   
    // draw the scene 
    this.renderer.resetGLState();
    this.renderer.render(this.scene, this.camera);

    //this.emitter.emit('requestRender');
    this.externalRenderers.requestRender(this.view);
    // cleanup
    context.resetWebGLState();
  }
}