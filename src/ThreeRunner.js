import * as THREE from 'three';
import Stats  from 'stats.js';
import { GUI } from 'dat.gui';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'; 
/**
 * ThreeRUnner
 *  
 * It sets up the Renderer, Scene and Camera. 
 * It also starts the render loop and handles window resizes.
 * @param container: 三维模型加载的canvas的上层容器
 * @param seedScene: 三维物体，主要是以组的形式
 */
export default class ThreeRunner {
  constructor(container, seedScene){
    if (container instanceof HTMLElement) {
      this.container= container;
    } else {
      // dom 
      this.container = document.createElement('div'); 
    }
    
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera();
    this.renderer = new THREE.WebGLRenderer({antialias: true});
    this.seedScene = seedScene;
    this.stats = new Stats();
    this.controls=null;
    this.axesHelper = new THREE.AxesHelper(500);
    this.guiParams = {
      ['三维模型']:'demo',
      ['加载进度']:0, 
      ['自动旋转'] : false,
      ['多选'] : 0,
      compute : () =>{
        this.seedScene.compute()
       }
    };  

    this.helper=null;
		this.raycaster = new THREE.Raycaster();
		this.mouse = new THREE.Vector2();
  }

  start(){
    this._init();
    this._animate();
    this._fullsize();
  }

  _init(){
    // scene
    this.scene.add(this.seedScene);
    //axis
    this.scene.add(this.axesHelper); 
        
    // camera
    //this.camera.position.z=30;
    this.camera.position.set( 1000, 50, 1500 );
    //camera.position.set(6,3,-10); 
    //this.camera.lookAt(new Vector3(0,0,0)); 
    this.camera.lookAt(this.scene.position);

    // renderer
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setClearColor(0x7ec0ee, 1);  

    //dom
    this.container.appendChild(this.renderer.domElement);  
    this.container.appendChild( this.stats.dom );
    window.addEventListener('resize', this._onWindowResizeHanlder,false);  

    this.controls= new OrbitControls(this.camera, this.renderer.domElement);
    //gui
    this._initGui();
  }
  _initGui(){  
    var gui = new GUI(); 
    gui.add(this.guiParams, '三维模型');
    gui.add(this.guiParams, '加载进度',0,100).listen();
    // gui.add(this.guiParams, '速度', -5, 5);
    gui.add(this.guiParams, '自动旋转');
    // gui.add(this.guiParams,'多选',{ Stopped: 0, Slow: 0.1, Fast: 5 })
    gui.add(this.guiParams, 'compute');

    this.seedScene.on('loadchanged',(step)=>{ 
      this.guiParams['加载进度']=step;
    }); 
  }

  _animate(){
    window.requestAnimationFrame(this._onAnimationFrameHandler);
  }

  _fullsize(){
    this._onWindowResizeHanlder();
  }
  // render loop
  _onAnimationFrameHandler = (timeStamp)=>{ 
    
    if (this.guiParams['自动旋转']){
      this.seedScene.update && this.seedScene.update(timeStamp);
    }

    this.stats.update();
    this.controls.update();

    this.renderer.render(this.scene, this.camera);
    window.requestAnimationFrame(this._onAnimationFrameHandler);

  }

  // resize
  _onWindowResizeHanlder = ()=>{ 
    const { clientWidth, clientHeight } = this.container;
    this.camera.aspect = clientWidth / clientHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(clientWidth, clientHeight);
    // const { innerHeight, innerWidth } = window;
    // this.camera.aspect = innerWidth / innerHeight;
    // this.camera.updateProjectionMatrix();
    // this.renderer.setSize(innerWidth, innerHeight);
  }  

}  