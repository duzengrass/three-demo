# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "余家2.tif", "余家2.tif"
import arcpy
from arcpy import env
from arcpy.sa import *

outCutFill=arcpy.CutFill_3d(in_before_surface="余家2.tif", in_after_surface="余家2.tif", out_raster="C:/Users/DUZENG/Documents/ArcGIS/Default.gdb/CutFill_tif1", z_factor="1")
print(outCutFill)