import { Vector3,BufferGeometry, Geometry } from 'three';

export default class Computer {
    static areaOfTriangle(p1,p2,p3){
        var v1 = new Vector3();
        var v2 = new Vector3();
        // 一顶点到另外两顶点的向量  
        v1 = p1.clone().sub(p2);
        v2 = p1.clone().sub(p3);
      
        var v3 = new Vector3();
        // 此向量是从向量1指向向量2 
        v3.crossVectors(v1,v2);
        var s = v3.length()/2;
        return s;
    }

    static volumeOfTetrahedron(p1,p2,p3){
        return p1.clone().cross(p2).dot(p3)/6;
    }

    static area(geometry){
        let tempArea=0;
        for (let i = 0; i < geometry.faces.length; i++) { 
            // 获得三角形对三个顶点的坐标
            const p1 = geometry.vertices[geometry.faces[i].a];
            const p2 = geometry.vertices[geometry.faces[i].b];
            const p3 = geometry.vertices[geometry.faces[i].c]; 
            tempArea += Computer.areaOfTriangle(p1, p2, p3);  
        }

        return tempArea;
    }

    static newVolume(geometry) {
        if (geometry instanceof BufferGeometry) {
            geometry = new Geometry().fromBufferGeometry(geometry);
        }
        //尺寸
        geometry.computeBoundingBox();
        let tempVolume=0;
        for (let i = 0; i < geometry.faces.length; i++) {  
            const p0 = geometry.vertices[geometry.faces[i].a];
            const p1 = geometry.vertices[geometry.faces[i].b];
            const p2 = geometry.vertices[geometry.faces[i].c]; 
            //网格三角形与坐标原点构成的四面体体积累加计算
            tempVolume += Computer.volumeOfTetrahedron(p0, p1, p2);
        }
        return tempVolume; 

    }
    
    static volume(geometry){
        let tempVolume=0;
        if (geometry instanceof Geometry){
            for (let i = 0; i < geometry.faces.length; i++) {  
                const p0 = geometry.vertices[geometry.faces[i].a];
                const p1 = geometry.vertices[geometry.faces[i].b];
                const p2 = geometry.vertices[geometry.faces[i].c]; 
                //网格三角形与坐标原点构成的四面体体积累加计算
                tempVolume += Computer.volumeOfTetrahedron(p0, p1, p2);
            }
            return tempVolume;
        } else if (geometry instanceof BufferGeometry) {
            //index存储的是每个三角面片的三个顶点的索引，为空则默认每三个连续的位置代表一个三角面片
            if (!geometry.index){
                let points=geometry.attributes.position; 
                for (let index = 0; index < points.count; index+=3) {
                    const p0 = new Vector3(points.getX(index),points.getY(index),points.getZ(index));
                    const p1 = new Vector3(points.getX(index+1),points.getY(index+1),points.getZ(index+1));
                    const p2 = new Vector3(points.getX(index+2),points.getY(index+2),points.getZ(index+2));
                    //网格三角形与坐标原点构成的四面体体积累加计算
                    tempVolume += Computer.volumeOfTetrahedron(p0, p1, p2);
                    
                } 
            }
            console.log('几何体数据结构',geometry);
            console.log('顶点位置、法向量、UV、颜色顶点等数据集合',geometry.attributes);
            console.log('顶点位置数据',geometry.attributes.position);
            console.log('顶点索引数据',geometry.index);

            return tempVolume;
        } else {
            throw new Error('geometry应当为Geometry或BufferGeometry类型!')
        }
      
    }
}  