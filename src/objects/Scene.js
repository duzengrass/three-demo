import * as THREE from 'three';
import Land from './Land/Land.js';
import Flower from './Flower/Flower.js';
import BasicLights from './Lights.js';
import GLTFModel from './GLTFModel/GLTFModel.js'
import OBJModel  from './OBJModel/OBJModel.js';
import FBXModel from './FBXModel/FBXModel.js';
import PLYModel from './PLYModel/PLYModel.js';

import BaseExternalModel from './BaseExternalModel';
import Computer from '../compute/computer';

const gltfurl='../../models/rocky_surface/gITF/scene.gltf';
const objurl='../../models/yujia/yujia-second/yujia_sencond_simplified_3d_mesh.obj';
const textureUrl='../../models/yujia/yujia-second/yujia_sencond_simplified_3d_mesh.mtl';
const fbxurl='../../models/yujia/yujia2_simplified_3d_mesh.fbx';
const plyurl='../../models/other/a.PLY';

export default class SeedScene extends BaseExternalModel {
  constructor() {
    super(); 
    this.externalModel=new PLYModel(plyurl); //new OBJModel(objurl,textureUrl);
    this.externalModel.init();
    this.externalModel.on('loadchanged', (step)=>{
      this.emitter.emit('loadchanged',step); 
    });  

    var geometry = new THREE.BoxGeometry();
    var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
    var cube = new THREE.Mesh( geometry, material ); 
      
    this.add(
      //cube,
      //new Land(), 
      //new Flower(), 
      //new GLTFModel(gltfurl),
      this.externalModel, 
      new BasicLights()
    );
    console.log(this.externalModel); 
  }

  update(timeStamp) {
    this.rotation.y = timeStamp / 10000;
  }

  compute(){
    console.log(`${this.externalModel.modelName}:${Computer.newVolume(this.externalModel.internalModel.geometry).toFixed(2)}`);
  }
} 