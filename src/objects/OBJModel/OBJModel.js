import { LoadingManager } from 'three'; 
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader';
 
import BaseExternalModel from '../BaseExternalModel';

export default class OBJModel extends BaseExternalModel {
  constructor(url,textTureUrl) { 
    var manager = new LoadingManager();
    var loader = new OBJLoader(manager);
    super('obj-model',url, loader); 
    this.textTureUrl=textTureUrl;
    this.manager = manager;
  }

  init(){ 
    if (this.textTureUrl){
      const mtlLoader=new MTLLoader();
      mtlLoader.load(this.textTureUrl,(materials)=>{
        console.log(`materials:${materials}`);
        this.loader.setMaterials(materials);
        this._loadObj();
      });
    } else {
      this._loadObj();
    }

    this.manager.onLoad=()=>{
      this.emitter && this.emitter.emit && this.emitter.emit(BaseExternalModel.LODE_COMPLETED); 
      console.log( 'loading complete' );
    }
    this.manager.onProgress =(url, itemsLoaded, itemsTotal)=>{
      console.log( 'Loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.' );
      //this.emitter && this.emitter.emit && this.emitter.emit(BaseExternalModel.LOAD_CHANGED,current);
    }
    this.manager.onError = (url) =>{
      console.log( 'There was an error loading ' + url );
    }
  }

  _loadObj(){
    this.loader.load(this.url,(object)=>{ 
      this.add(object);
      this._centerObj(object);
    }, ( xhr )=>{ 
      const current= xhr.loaded / xhr.total * 100;
      this.emitter && this.emitter.emit && this.emitter.emit(BaseExternalModel.LOAD_CHANGED,current);
      console.log( current+ '% loaded' );
    },(error)=>{
      console.error(`An error happened:${error}`);
    });
  }

  _centerObj(object){
    this.internalModel=object.children[0];
    this.internalModel.geometry.center();
  }

} 
