import { Group, ObjectLoader  } from 'three'; 
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader'; 
//const url='../../../models/quarry/quarry.obj';
import EventEmitter from 'events';

export default class BaseExternalModel extends Group {
  static LOAD_CHANGED='loadchanged';
  static LODE_COMPLETED='load_completed';
  constructor(name,url,loader) {  
    super();

    this.name = name; 
    this.url=url;
    this.loader=loader; 
    this.emitter = new EventEmitter();
    this.on=this.emitter.on.bind(this.emitter);
    this.off=this.emitter.off.bind(this.emitter); 
  }
 
  get modelName(){
    return this.name;
  }

  get model(){
    return this.internalModel;
  }
  
  init(){

  }
}
