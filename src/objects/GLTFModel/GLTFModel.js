import { Group, ObjectLoader  } from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';  
import BaseExternalModel from '../BaseExternalModel';

export default class GLTFModel extends BaseExternalModel {
  constructor(url) {  
    super('gltf-model',url, new GLTFLoader()); 
  }

  init(){
    this.loader.load(this.url,(gltf)=>{
      this.add(gltf.scene)
    }, ( xhr )=>{
      const current= xhr.loaded / xhr.total * 100;
      this.emitter && this.emitter.emit && this.emitter.emit(BaseExternalModel.LOAD_CHANGED,current)
      console.log( current+ '% loaded' );
    },(error)=>{
      console.error(`An error happened:${error}`);
    });
  }
} 
