import { Color, Mesh } from 'three';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'; 
import BaseExternalModel from '../BaseExternalModel';

export default class FBXModel extends BaseExternalModel {
  constructor(url) {  
    super('fbx-model',url, new FBXLoader());  
  }

  init(){ 
    this.loader.load(this.url,(object)=>{ 
        object.traverse(function(child){
            if(child instanceof Mesh){
                child.material.emissive=new Color(1,1,1);
                child.material.emissiveIntensity=1;
                child.material.emissiveMap=child.material.map;
            } 
        });
        this.add(object)
      }, ( xhr )=>{
        const current= xhr.loaded / xhr.total * 100;
        this.emitter && this.emitter.emit && this.emitter.emit(BaseExternalModel.LOAD_CHANGED,current);
        console.log( current+ '% loaded' );
      },(error)=>{
        console.error(`An error happened:${error}`);
      });
  } 
} 
