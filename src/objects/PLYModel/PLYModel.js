import { Color, Mesh,MeshStandardMaterial } from 'three';
import { PLYLoader } from 'three/examples/jsm/loaders/PLYLoader'; 
import BaseExternalModel from '../BaseExternalModel';

export default class PLYModel extends BaseExternalModel {
  constructor(url) {  
    super('ply-model',url, new PLYLoader());  
  }

  init(){ 
    this.loader.load(this.url,(geometry)=>{ 
        geometry.computeVertexNormals();
        const material = new MeshStandardMaterial( { color: 0x0055ff, flatShading: true } );
        const mesh = new Mesh( geometry, material );

        mesh.position.y = - 0.2;
        mesh.position.z = 0.3;
        mesh.rotation.x = - Math.PI / 2;
        mesh.scale.multiplyScalar( 0.001 );

        mesh.castShadow = true;
        mesh.receiveShadow = true;

        this.add(mesh)

      }, ( xhr )=>{
        const current= xhr.loaded / xhr.total * 100;
        this.emitter && this.emitter.emit && this.emitter.emit(BaseExternalModel.LOAD_CHANGED,current);
        console.log( current+ '% loaded' );
      },(error)=>{
        console.error(`An error happened:${error}`);
      });
  } 
} 
