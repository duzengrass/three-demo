/**
 * entry.js
 * 
 * This is the first file loaded. It sets up the Renderer, 
 * Scene and Camera. It also starts the render loop and 
 * handles window resizes.
 * 
 */

import { WebGLRenderer, PerspectiveCamera, Scene, Vector3 } from 'three';
import Stats  from 'stats.js';
import SeedScene from '../objects/Scene.js';

const scene = new Scene();
const camera = new PerspectiveCamera();
const renderer = new WebGLRenderer({antialias: true});
const seedScene = new SeedScene();
const stats = new Stats();
// scene
scene.add(seedScene);

// camera
camera.position.z=30;
//camera.position.set( 1000, 50, 1500 );
 //camera.position.set(6,3,-10); 
// camera.lookAt(new Vector3(0,0,0)); 

// renderer
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setClearColor(0x7ec0ee, 1);

// render loop
const onAnimationFrameHandler = (timeStamp) => {
  renderer.render(scene, camera);
  seedScene.update && seedScene.update(timeStamp);
  stats.update();
  window.requestAnimationFrame(onAnimationFrameHandler);
}
window.requestAnimationFrame(onAnimationFrameHandler);

// resize
const onWindowResizeHanlder = () => { 
  const { innerHeight, innerWidth } = window;
  camera.aspect = innerWidth / innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(innerWidth, innerHeight);
};
//onWindowResizeHanlder();
//window.addEventListener('resize', onWindowResizeHanlder,false);

// dom
document.body.style.margin = 0;
//document.body.appendChild( renderer.domElement );
const container = document.createElement('div');
document.body.appendChild(container);
container.appendChild(renderer.domElement);  


container.appendChild( stats.dom );
window.addEventListener('resize', onWindowResizeHanlder,false);



